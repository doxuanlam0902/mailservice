package com.example.MailService;

import com.example.MailService.Consume.ConsumerConfig;
import com.rabbitmq.client.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
public class MailServiceApplication {

	public static ApplicationContext context ;

	@Autowired
	public static ConsumerConfig consumerConfig = new ConsumerConfig() ;

	public static void main(String[] args) throws IOException, TimeoutException {
		context = SpringApplication.run(MailServiceApplication.class, args);
//		System.out.println(context.getBean(JavaMailSender.class)) ;
		System.out.println("Send Mail :");
		consumerConfig.getMessage() ;
	}

}
