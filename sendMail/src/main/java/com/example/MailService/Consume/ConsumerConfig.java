package com.example.MailService.Consume;


import com.example.MailService.MailServiceApplication;
import com.example.MailService.config.MailConfig;
import com.rabbitmq.client.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeoutException;

@Component
public class ConsumerConfig {

    public static String mailTo = "18020749@vnu.edu.vn" ;


    @Autowired
    public JavaMailSender javaMailSender ;

    public void getMessage() throws IOException, TimeoutException {
//        System.out.println("Create a ConnectionFactory");
        javaMailSender = MailServiceApplication.context.getBean(JavaMailSender.class);

        ConnectionFactory factory = new ConnectionFactory();

//        factory.setHost("192.168.50.163");
        factory.setHost("127.0.0.1");
        factory.setPort(5672);
        factory.setUsername("guest");
        factory.setPassword("guest");

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        while (true) {
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), "UTF-8");
                System.out.println(" [x] Received: '" + message + "'");
                SimpleMailMessage mail = new SimpleMailMessage();
                mail.setTo(mailTo);
                mail.setSubject("Test Simple Email");
                mail.setText(message);
                // Send Message!
                javaMailSender.send(mail);
            };
            CancelCallback cancelCallback = consumerTag -> {
            };
            String consumerTag = channel.basicConsume("message_queue", true, deliverCallback, cancelCallback);

        }

    }
}
